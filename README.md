# URL Shortener Microservice

Here you could see it in action: https://just-another-url-shortener.herokuapp.com

Unfortunately, as Heroku basically cancelled their free plan, the online demo is currently in the process of migrating to a new home.

![Urlshort](URL_shortener.png)

